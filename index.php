<?php
include_once './mod/load.php';

$q = index\db\prepare("SELECT * FROM entries WHERE html LIKE ?");

?>
<style type="text/css">
  #search {
    text-align: center;
  }
</style>
<script type="text/javascript">

</script>

<?=index\gui\search($_GET['q'])?>

<ol id=results>
<?php
if (isset($_GET['q']) && $q->execute(["%{$_GET['q']}%"]))
  while ($var = $q->fetch())
    print \index\gui\result($var);
?>
</ol>
