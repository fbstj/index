<?php
# index\gui base module
namespace index\gui;

function search($query="")
{
    ob_start(); ?>
<!-- start index\gui\search -->
<form id=search>
  <input type="text" name="q" value="<?=$query?>">
  <button>Search</button>
</form>
<!-- end index\gui\search -->
<?php   return ob_get_clean();
}

function result($info)
{
    $id = $info['id'];
    $html = $info['html'];
    $url = $info['url'];
    
    ob_start(); ?>
<!-- start index\gui\result -->
<li value="<?=$id?>" id="<?=$id?>">
    <?=$html?>
    <cite><a href="<?=$url?>">[cite]</a></cite>
</li>
<!-- end index\gui\search -->
<?php   return ob_get_clean();
}
