<?php
# index\db base module
namespace index\db;

$db = NULL;

function start($name)
{
	global $db;
	$db = new \PDO($name);
}

function prepare($query)
{
	global $db;
	return $db->prepare($query);
}

function table_exists($name)
{
	global $db;
	$res = $db->query("SELECT * FROM \"modules\" WHERE \"name\" = \"{$name}\"");
	foreach ($res as $row)
	{
		return true;
	}
	return false;
}

namespace index\db\install;

function create_table($name, $fields = [], $version)
{
	if (\index\db\table_exists($name))
		return;
	global $db;
	$flds = implode(',', $fields);
	$qry = "CREATE TABLE IF NOT EXISTS \"{$name}\" ({$flds})";
	$db->exec($qry);
	$qry = 'INSERT INTO "modules" ("name", "version") VALUES ("'.$name.'", "'.$version.'");';
	$db->exec($qry);
}
